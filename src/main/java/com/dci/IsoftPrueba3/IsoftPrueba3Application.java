package com.dci.IsoftPrueba3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsoftPrueba3Application {

	public static void main(String[] args) {
		SpringApplication.run(IsoftPrueba3Application.class, args);
	}

}
